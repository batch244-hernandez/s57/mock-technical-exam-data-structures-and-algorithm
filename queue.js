let collection = [];

// // Write the queue functions below.

// Print queue elements.
function print() {
	return collection;
};


// Enqueue a new element.
function enqueue(element) {
    collection[collection.length] = element;
	return collection;    
};


// Dequeue the first element.
function dequeue() {
	
	let newCollection = [];

	for (let i = 1; i < collection.length; i++) {
		newCollection[i-1] = collection[i];

		collection = newCollection;		
	}
	return collection;
};


// Get first element.
function front() {
	return collection[0];
};


// Get queue size.
function size() {
	return collection.length;
};


// Check if queue is not empty.
function isEmpty() {
	return collection.length === 0;
};



module.exports = {
	collection,
	print,
	enqueue,
	dequeue,
	front,
	size,
	isEmpty
};